while IFS="" read -r p || [ -n "$p" ]
do 
    filename=$(echo $p | tr ":" "-" | rev | cut -d'/' -f 1 | rev )
    echo $filename
    podman pull $p
    sudo docker pull $p
    podman save -o $filename.tar $p
done <containers.txt
